# FullFake

This command line app resizes the main window of the processes with the given names to fill the whole screen or the
desktop area (the screen without the taskbar).

To resize the window of the "MyGame" process (no matter the extension) to fill the whole screen, run it as follows:
```
FULLFAKE MyGame
```
To keep the taskbar visible, additionally pass "/t" or "/taskbar":
```
FULLFAKE MyGame /t
```
You can pass as many process names as you like, it will resize all found windows.
```
FULLFAKE MyGame AnotherGame LowResGame /t
```

Note that you may have to run it with administrator rights.