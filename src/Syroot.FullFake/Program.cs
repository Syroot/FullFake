﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using static Syroot.Windows.User32;

namespace Syroot.FullFake
{
    internal class Program
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static RECT _rect;
        private static List<string> _processNames;

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main(string[] args)
        {
            try
            {
                ParseArguments(args);

                // Get the processes and window handles (requires administrator rights).
                foreach (string processName in _processNames)
                {
                    Process[] processes = Process.GetProcessesByName(processName);
                    foreach (Process process in processes)
                    {
                        IntPtr hWnd = process.MainWindowHandle;

                        // Get the window style and remove the border.
                        WindowStyle style = (WindowStyle)GetWindowLongPtr(hWnd, WindowLongQuery.GWL_STYLE).ToInt64();
                        style &= ~(WindowStyle.WS_CAPTION | WindowStyle.WS_THICKFRAME | WindowStyle.WS_MINIMIZE
                            | WindowStyle.WS_MAXIMIZE | WindowStyle.WS_SYSMENU);

                        // Set the new style.
                        SetWindowLongPtr(hWnd, WindowLongQuery.GWL_STYLE, (IntPtr)style);

                        // Resize the window to fill the desktop.
                        SetWindowPos(hWnd, IntPtr.Zero, _rect.left, _rect.top, _rect.Width,
                            _rect.Height, SetWindowPosFlags.SWP_FRAMECHANGED | SetWindowPosFlags.SWP_NOZORDER);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
#if DEBUG
                Console.ReadLine();
#endif
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void ParseArguments(string[] args)
        {
            _rect = new RECT(0, 0,
                GetSystemMetrics(SystemMetric.SM_CXSCREEN), GetSystemMetrics(SystemMetric.SM_CYSCREEN));
            _processNames = new List<string>();

            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i].ToUpperInvariant();
                if (arg.StartsWith("/t", StringComparison.OrdinalIgnoreCase))
                    SystemParametersInfo(SysParameter.SPI_GETWORKAREA, 0, ref _rect, SysParameterChange.None);
                else
                    _processNames.Add(Path.GetFileNameWithoutExtension(arg));
            }

            if (_processNames.Count == 0)
                throw new Exception("No process name was passed.");
        }
    }
}
